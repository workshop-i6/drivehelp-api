package com.workshop.heldrive.api.security;

import com.workshop.heldrive.api.Services.UserAuthenticationService;
import com.workshop.heldrive.api.Services.UserCrudService;
import com.workshop.heldrive.api.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


import java.util.Objects;
import java.util.Optional;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@Service
@AllArgsConstructor(access = PACKAGE)
@FieldDefaults(level = PRIVATE,makeFinal = true)
public class UUIDAuthenticationService implements UserAuthenticationService {

    @NonNull
    UserCrudService userCrudService;
    private final Logger logger= Logger.getLogger(UUIDAuthenticationService.class);

    @Override
    public Optional<String> login(String mail, String password) {
        Optional<User> userOptio = userCrudService.findByMail(mail).filter(user-> Objects.equals(password,user.getPassword()));
        if(userOptio.isPresent()){
            User user=userOptio.get();
            String uuidToken=  userCrudService.generateToken(user);
            return Optional.of(uuidToken);
        }else{
           return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByToken(String token) {
        return userCrudService.findByToken(token);
    }

    @Override
    public void logout(User user) {
        user.setToken(null);
        user.setExpire(null);
        userCrudService.save(user);
    }
}
