package com.workshop.heldrive.api.controller;

import com.workshop.heldrive.api.Repository.CommandeRepository;
import com.workshop.heldrive.api.Services.CommandeService;
import com.workshop.heldrive.api.Services.UserService;
import com.workshop.heldrive.api.entity.beans.CommandeBean;
import com.workshop.heldrive.api.entity.beans.LivreurCommande;
import com.workshop.heldrive.api.entity.model.Commande;
import com.workshop.heldrive.api.entity.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("commandes")
@CrossOrigin(origins = "*")
public class CommandeController {

    @Autowired
    private CommandeRepository commandeRepository;
    private final Logger logger= Logger.getLogger(CommandeController.class);

    @Autowired
    private CommandeService commandeService;
    @Autowired
    private UserService userService;

    /**
     * @return toutes les commandes
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value ="",produces = "application/json")
    public Collection<Commande> getAll(){
        logger.info("all commandes");
        Collection<Commande> commandes=new ArrayList<>((Collection<?extends Commande>)commandeRepository.findAll());
        return commandes;
    }


    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/{id}",produces = "application/json")
    public String deleteCommande(@PathVariable int id){
        return commandeService.deleteCommande(id);
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value ="/demandeur/{id}",produces = "application/json")
    public Collection<Commande> getAllCommandesDemandeur(@PathVariable  int id){
        logger.info("all commandes demadneurs");
        return new ArrayList<>((Collection<? extends Commande>)commandeRepository.findByDemandeurId(id));
    }


    @CrossOrigin(origins = "*")
    @GetMapping(value ="/livreur/{id}",produces = "application/json")
    public Collection<Commande> getAllCommandesLivreur(@PathVariable  int id){
        logger.info("all commandes livreur");
        return new ArrayList<>((Collection<? extends Commande>) commandeRepository.findByDemandeurId(id));
    }


    @CrossOrigin(origins = "*")
    @PostMapping(value = "/create",produces = "application/json")
    public Commande createCommande(@RequestBody CommandeBean commandeBean) throws Exception {
            logger.info(commandeBean);
            Commande commande=commandeService.generateCommande(commandeBean);
            return commande;
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "accept",produces = "application/json")
    public Commande addLivreur(@RequestBody LivreurCommande livreurCommande) throws Exception {
        Commande save = commandeService.addLivreurCommande(livreurCommande);
        return save;
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "end/{id}",produces = "application/json")
    public String endCommande(@PathVariable int id) throws Exception {
        commandeService.endCommande(id);
        return "{\"message\":\"commande terminé\"}";
    }


}
