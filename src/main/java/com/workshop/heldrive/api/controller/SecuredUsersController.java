package com.workshop.heldrive.api.controller;



import com.workshop.heldrive.api.Repository.UserRepository;
import com.workshop.heldrive.api.Services.UserAuthenticationService;
import com.workshop.heldrive.api.Services.UserService;
import com.workshop.heldrive.api.entity.DataUser;
import com.workshop.heldrive.api.entity.Userlogin;
import com.workshop.heldrive.api.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;


@RestController
@RequestMapping("users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@CrossOrigin(origins = "*")
public class SecuredUsersController {

    @NonNull
    private UserAuthenticationService authenticationService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/current",produces = "application/json")
    public Userlogin getCurrent(@AuthenticationPrincipal User user){
        Userlogin userlog=new Userlogin(user);
        return  userlog;
    }

    @CrossOrigin(value = "*")
    @GetMapping(value = "/{id}",produces = "application/json")
    public User getUser(@PathVariable int id) throws Exception {
        return userService.getUserById(id);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/logout")
    public boolean logout(@AuthenticationPrincipal User user){
        authenticationService.logout(user);
        return true;
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "all",produces = "application/json")
    public Collection<User> getAll(){
         return new ArrayList<>((Collection<? extends User>) userRepository.findAll());
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/datauser",produces = "application/json")
    public DataUser getAllDataUser(@AuthenticationPrincipal User user){
        return userService.getDataUser(user);
    }


}
