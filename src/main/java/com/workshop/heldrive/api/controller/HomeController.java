package com.workshop.heldrive.api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;


/**
 * Controlleur page par défaut
 */
@RestController
public class HomeController {

    /**
     * rediction vers la documentation de l'api
     * @return Redirectview
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/public")
    public RedirectView HomePage() {
        return new RedirectView("swagger-ui.html");
    }
}
