package com.workshop.heldrive.api.controller;

import com.workshop.heldrive.api.Repository.ProduitRepository;
import com.workshop.heldrive.api.entity.Message;
import com.workshop.heldrive.api.entity.beans.ProduitBean;
import com.workshop.heldrive.api.entity.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;


@RestController
@RequestMapping(value="produits")
@CrossOrigin(origins = "*")
public class ProduitController {

    @Autowired
    private ProduitRepository produitRepository;
    private final Logger logger= Logger.getLogger(ProduitController.class);

    /**
     * @return la liste de produits
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value="",produces = "application/json")
    public Collection<Produit> getAllProduits(){
        logger.info("produits");
        return new ArrayList<>((Collection<?extends Produit>)produitRepository.findAll());
    }

    /**
     * @param id identifiant du produit
     * @return le produit correspondant à l'Id
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/{id}",produces = "application/json")
    public Produit getProduitById(@PathVariable int id){
        Optional<Produit> produitOption=produitRepository.findById(id);
        Produit produit=null;
        if(produitOption.isPresent()){
            produit=produitOption.get();
        }
        return produit;
    }

    /**
     * @param produitbean le produit à enregistré
     * @return Le produit sauvegardé
     */
    @CrossOrigin(origins = "*")
    @PostMapping(value = "/create",produces = "application/json")
    public Produit saveProduit(@RequestBody ProduitBean produitbean){
        Produit produit=new Produit(produitbean);
        return produitRepository.save(produit);
    }


    /**
     * @param id Produit a supprimée
     * @return Message
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/{id}",produces = "application/json")
    public Message deleteProduit(@PathVariable int id){
        Optional<Produit> produitOpt=produitRepository.findById(id);
        String mess="échec de la suppression";
        if(produitOpt.isPresent()){
            produitRepository.deleteById(id);
           mess="Produit supprimé";
        }
        return new Message(mess);
    }


}
