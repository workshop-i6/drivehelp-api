package com.workshop.heldrive.api.controller;

import com.workshop.heldrive.api.Repository.CategorieRepository;
import com.workshop.heldrive.api.Repository.ProduitRepository;
import com.workshop.heldrive.api.entity.Message;
import com.workshop.heldrive.api.entity.beans.CategorieBean;
import com.workshop.heldrive.api.entity.beans.ProduitBean;
import com.workshop.heldrive.api.entity.model.Categorie;
import com.workshop.heldrive.api.entity.model.Produit;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(value = "categories")
@CrossOrigin(origins = "*")
public class CategorieController {


    @Autowired
    private CategorieRepository categorieRepository;
    private final Logger logger= Logger.getLogger(CategorieController.class);

    /**
     * @return la liste de produits
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value="",produces = "application/json")
    public Collection<Categorie> getAllProduits(){
        logger.info("produits");
        return new ArrayList<>((Collection<?extends Categorie>)categorieRepository.findAll());
    }

    /**
     * @param id identifiant du produit
     * @return le produit correspondant à l'Id
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/{id}",produces = "application/json")
    public Categorie getProduitById(@PathVariable int id){
        Optional<Categorie> categorieOpt=categorieRepository.findById(id);
        Categorie categorie=null;
        if(categorieOpt.isPresent()){
            categorie=categorieOpt.get();
        }
        return categorie;
    }

    /**
     * @param categorieBean le produit à enregistré
     * @return Le produit sauvegardé
     */
    @CrossOrigin(origins = "*")
    @PostMapping(value = "/create",produces = "application/json")
    public Categorie saveProduit(@RequestBody CategorieBean categorieBean){
        Categorie categorie=new Categorie(categorieBean);
        Categorie save = categorieRepository.save(categorie);
        return save;
    }

    /**
     * @param id l'identifiant du produit à supprimé
     * @return
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/{id}",produces = "application/json")
    public Message deleteProduit(@PathVariable int id){
        Optional<Categorie> produitOpt=categorieRepository.findById(id);
        String mess="échec de la suppression";
        if(produitOpt.isPresent()){
            categorieRepository.deleteById(id);
            mess="Produit supprimé";
        }
        Message message=new Message(mess);
        return message;
    }


}
