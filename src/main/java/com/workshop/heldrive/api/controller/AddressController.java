package com.workshop.heldrive.api.controller;

import com.workshop.heldrive.api.Repository.AddressRepository;
import com.workshop.heldrive.api.Services.AddressService;
import com.workshop.heldrive.api.entity.beans.AddressBean;
import com.workshop.heldrive.api.entity.model.Address;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping(value = "address")
@CrossOrigin("*")
public class AddressController {

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private AddressService addressService;

    private final Logger log=Logger.getLogger(AddressController.class);

    /**
     * @return la liste des adresses
     */
    @GetMapping(value = "",produces = "application/json")
    public Collection<Address> getAll(){
        return  new ArrayList<>((Collection<? extends Address>) addressRepository.findAll());
    }

    /**
     * @param id identifiant du demadneur de la commande
     * @return les adresses du demadneurs
     */
    @GetMapping(value = "/demandeur/{id}",produces = "application/json")
    public Collection<Address> getAddressDemandeur(int id){
        return new ArrayList<>((Collection<? extends Address>)addressRepository.getAddressByIdUser(id));
    }

    /**
     * @param addressBean l'adresse du client à sauvegardé
     * @return si l'adresse a été sauvegardé.
     * @throws Exception
     */
    @PostMapping(value = "/create",produces = "application/json")
    public String saveAddress(@RequestBody AddressBean addressBean) throws Exception {
        return addressService.save(addressBean);
    }



}
