package com.workshop.heldrive.api.controller;

import com.workshop.heldrive.api.Services.UserAuthenticationService;
import com.workshop.heldrive.api.Services.UserCrudService;
import com.workshop.heldrive.api.entity.Message;
import com.workshop.heldrive.api.entity.Userlogin;
import com.workshop.heldrive.api.entity.beans.Login;
import com.workshop.heldrive.api.entity.beans.UserBean;
import com.workshop.heldrive.api.entity.model.User;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/public/users")
@FieldDefaults(level= PRIVATE,makeFinal = true)
@AllArgsConstructor(access =PACKAGE)
@CrossOrigin(origins = "*")
public class UserController {

    @NonNull
    UserAuthenticationService authenticationService;

    @NonNull
    UserCrudService userCrudService;

    private final Logger logger= Logger.getLogger(UserController.class);

    @CrossOrigin(origins = "*")
    @PostMapping(value="/login",produces = "application/json")
    public String login(@RequestBody Login login){
        logger.info("login");
        String password= DigestUtils.sha256Hex(login.getPassword());
        String mail=login.getMail();
        return "{\"token\":\""+authenticationService.login(mail,password).orElseThrow(()->new RuntimeException("invalid login and/or password"))+"\"}";
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value="/create",produces = "application/json")
    public String createUser(@RequestBody  UserBean userBean){
        logger.info(userBean);
        String password = userBean.getPassword();
        password= DigestUtils.sha256Hex(password);
        User user = new User(userBean);
        user.setPassword(password);
        user=userCrudService.save(user);
        return "{\"message\":\"Utilisateur créer \"}";
    }




}
