package com.workshop.heldrive.api.Services;

import com.workshop.heldrive.api.Repository.AddressRepository;
import com.workshop.heldrive.api.Repository.CityRepository;
import com.workshop.heldrive.api.Repository.StateRepository;
import com.workshop.heldrive.api.Utilty.StringUtils;
import com.workshop.heldrive.api.entity.beans.AddressBean;
import com.workshop.heldrive.api.entity.model.Address;
import com.workshop.heldrive.api.entity.model.City;
import com.workshop.heldrive.api.entity.model.State;
import com.workshop.heldrive.api.entity.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private UserService userService;


    private Logger logger=Logger.getLogger(AddressService.class);

    public String save(AddressBean addressBean) throws Exception {
        addressBean.setCountry(StringUtils.captalizeFirstLetter(addressBean.getCountry()));
        State state=new State(addressBean);
        try {
            state=stateRepository.save(state);
        }catch (DataIntegrityViolationException e){
            state=stateRepository.findByName(state.getName()).get();
        }

        addressBean.setCity(StringUtils.captalizeFirstLetter(addressBean.getCity()));
        City city=new City(addressBean);
        city.setState(state);
        try {
            city=cityRepository.save(city);
        }catch (DataIntegrityViolationException e){
            city=cityRepository.findBydZipCode(city.getZipcode()).get();
        }

        User user=userService.getUserById(addressBean.getUserId());
        Address address=new Address(addressBean);
        address.setUser(user);
        address.setCity(city);
        address=addressRepository.save(address);
        return "{\"adresse\":\"sauvegarde\"}";
    }
}
