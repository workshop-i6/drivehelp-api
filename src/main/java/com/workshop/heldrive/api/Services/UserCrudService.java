package com.workshop.heldrive.api.Services;

import com.workshop.heldrive.api.entity.model.User;

import java.util.Optional;

public interface UserCrudService {

    User save(User user);
    Optional<User> find(int id);
    Optional<User> findByMail(String username);
    Optional<User> findByToken(String token);
    String generateToken(User user);
}
