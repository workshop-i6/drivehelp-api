package com.workshop.heldrive.api.Services;

import com.workshop.heldrive.api.Repository.CommandeRepository;
import com.workshop.heldrive.api.Repository.UserRepository;
import com.workshop.heldrive.api.entity.DataUser;
import com.workshop.heldrive.api.entity.model.Commande;
import com.workshop.heldrive.api.entity.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements UserCrudService {

    @Autowired
    private UserRepository userRepository;
    private Logger log=Logger.getLogger(UserService.class);

    @Autowired
    private CommandeRepository commandeRepository;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> find(int id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByMail(String mail) {
        Collection<User> users = getUsers();
        Optional<User> first = users.stream().filter(u -> Objects.equals(mail, u.getEmail())).findFirst();
        return first;
    }

    public User getUserById(int id) throws Exception {
        Optional<User> userOpt = userRepository.findById(id);
        if(!userOpt.isPresent()){
            throw new Exception("User not found");
        }
        return userOpt.get();
    }

    private Collection<User> getUsers() {
        return new ArrayList<>((Collection<? extends User>) userRepository.findAll());
    }

    public DataUser getDataUser(User user){
        DataUser dataUser=new DataUser();
        dataUser.setUser(user);
        Collection<Commande> commandesDemandeurs=new ArrayList<>((Collection<? extends Commande>) commandeRepository.findByDemandeurId(user.getId()));
        Collection<Commande> commandesLiveurs=new ArrayList<>((Collection<? extends Commande>)commandeRepository.findByLivreurId(user.getId()));
        dataUser.setCommandesDemandeur(commandesDemandeurs);
        dataUser.setCommandesLivreur(commandesLiveurs);
        return dataUser;
    }

    @Override
    public Optional<User> findByToken(String token) {
        log.info(token);
        Collection<User> users= getUsers();
        long now = new Date().getTime();
        return users.stream().filter(u->token.equals(u.getToken())&&u.getExpire().getTime()>now).findFirst();
    }

    @Override
    public String generateToken(User user){
        String uuidToken= UUID.randomUUID().toString();
        Date date=new Date();
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY,2);
        user.setExpire(calendar.getTime());
        user.setToken(uuidToken);
        save(user);
        return uuidToken;
    }
}
