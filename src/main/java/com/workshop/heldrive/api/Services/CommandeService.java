package com.workshop.heldrive.api.Services;

import com.workshop.heldrive.api.Repository.CommandeRepository;
import com.workshop.heldrive.api.Repository.LigneCommandeRepository;
import com.workshop.heldrive.api.Repository.ProduitRepository;
import com.workshop.heldrive.api.entity.beans.CommandeBean;
import com.workshop.heldrive.api.entity.beans.LivreurCommande;
import com.workshop.heldrive.api.entity.beans.ProduitComandeBean;
import com.workshop.heldrive.api.entity.model.Commande;
import com.workshop.heldrive.api.entity.model.LigneCommande;
import com.workshop.heldrive.api.entity.model.Produit;
import com.workshop.heldrive.api.entity.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;


@Service
public class CommandeService {

    @Autowired
    private LigneCommandeRepository ligneCommandeRepository;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ProduitRepository produitRepository;

    private Logger log= Logger.getLogger(CommandeService.class);

    public Commande generateCommande(CommandeBean commandeBean) throws Exception {
        User user=userService.getUserById(commandeBean.getDemandeur());
        Commande commande=new Commande();
        commande.setDemandeur(user);
        LocalDateTime now=LocalDateTime.now();
        String numero=generateNumeroCommande(now);
        commande.setNumero(numero);
        commande.setDate(now);
        commande=commandeRepository.save(commande);
        log.info(commande);
        commande=totalCommande(commandeBean,commande);
        commande=commandeRepository.save(commande);
        return commande;
    }

    private Commande totalCommande(CommandeBean commande,Commande comSave) {
        Collection<ProduitComandeBean> lines=commande.getLines();
        Collection<LigneCommande> lignes=new ArrayList<>();
        double sum=0.0;
        for ( ProduitComandeBean line:lines) {
            Produit prod=produitRepository.findById(line.getId()).get();
            LigneCommande l=new LigneCommande(line);
            l.setProduit(prod);
            l.setCommande(comSave);
            l=ligneCommandeRepository.save(l);
            lignes.add(l);
            sum+=l.getQuantite()*l.getProduit().getPrixMax();
        }
        comSave.setTotal(sum);
        comSave.setLines(lignes);
        return comSave;
    }

    private String generateNumeroCommande(LocalDateTime date) {
        String num="";
        num+=String.valueOf(date.getYear())+
                String.valueOf(date.getMonthValue())+String.valueOf(date.getDayOfMonth())+String.valueOf(date.getHour())+
                String.valueOf(date.getMinute())+String.valueOf(date.getSecond())+"-"+ UUID.randomUUID().toString();
        return num;
    }

    public Commande getCommandeById(int id) throws Exception {
        Optional<Commande> comOpt = commandeRepository.findById(id);
        if(!comOpt.isPresent()){
            throw new Exception("Commande not found!");
        }
        return comOpt.get();
    }

    private Date getDate() {
        Calendar now=Calendar.getInstance();
        TimeZone tz=TimeZone.getTimeZone("Europe/Paris");
        now.setTimeZone(tz);
        return now.getTime();
    }

    public String deleteCommande(int id) {
        commandeRepository.deleteById(id);
        return "{\"message\":\"reussi\"}";
    }

    public Commande addLivreurCommande(LivreurCommande livreurCommande) throws Exception {
        Commande commande=getCommandeById(livreurCommande.getIdCommande());
        User livreur=userService.getUserById(livreurCommande.getIdLivreur());
        commande.setLivreur(livreur);
        commande.setStatus(1);
        Commande save = commandeRepository.save(commande);
        return save;
    }

    public boolean endCommande(int id) throws Exception {
        Commande commande=getCommandeById(id);
        commande.setStatus(2);
        Commande save = commandeRepository.save(commande);
        return true;
    }
}
