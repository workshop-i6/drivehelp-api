package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.Address;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

    @Query("select a from Address a where a.user.id=?1")
    Iterable<Address> getAddressByIdUser(int id);
}
