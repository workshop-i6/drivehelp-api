package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.Commande;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CommandeRepository extends CrudRepository<Commande,Integer> {

    @Query("select c from Commande c where c.demandeur.id=?1 ")
    Iterable<Commande> findByDemandeurId(int id);
    @Query("select c from Commande c where c.livreur.id=?1 ")
    Iterable<Commande> findByLivreurId(int id);



}
