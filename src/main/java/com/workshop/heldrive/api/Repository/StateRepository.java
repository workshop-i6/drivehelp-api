package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.State;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StateRepository extends CrudRepository<State,Integer> {

    @Query("select s from State s where s.name=?1")
    Optional<State> findByName(String str);
}
