package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.Produit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduitRepository extends CrudRepository<Produit,Integer> {
}
