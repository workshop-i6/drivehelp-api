package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.City;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CityRepository extends CrudRepository<City,Integer> {

    @Query("select c from City c where c.zipcode=?1")
    Optional<City> findBydZipCode(String str);
}
