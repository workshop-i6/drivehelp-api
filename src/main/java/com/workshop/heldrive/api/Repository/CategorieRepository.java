package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.Categorie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie,Integer> {
}
