package com.workshop.heldrive.api.Repository;

import com.workshop.heldrive.api.entity.model.LigneCommande;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneCommandeRepository extends CrudRepository<LigneCommande,Integer> {
}
