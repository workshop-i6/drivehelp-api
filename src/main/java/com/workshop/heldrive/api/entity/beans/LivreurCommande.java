package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class LivreurCommande implements Serializable {

    private int idLivreur;
    private int idCommande;


    public LivreurCommande() {    }

    public LivreurCommande(int idLivreur, int idCommande) {
        this.idLivreur = idLivreur;
        this.idCommande = idCommande;
    }

    public int getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(int idLivreur) {
        this.idLivreur = idLivreur;
    }

    public int getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(int idCommande) {
        this.idCommande = idCommande;
    }



}
