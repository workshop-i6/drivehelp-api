package com.workshop.heldrive.api.entity.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workshop.heldrive.api.entity.beans.AddressBean;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "Address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "numero")
    private String numero;
    @Basic
    @Column(name = "street")
    private String street;

    @Basic
    @Column(name = "complement")
    private String complement;

    @OneToOne
    @JoinColumn(name="user_id",referencedColumnName = "id")
    @JsonIgnore
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @ManyToOne
    @JoinColumn(name = "city_id",referencedColumnName = "id")
    private City city;

    public Address() {
    }

    public Address(AddressBean addressBean) {
        numero=addressBean.getNumero();
        street=addressBean.getStreet();

    }











}
