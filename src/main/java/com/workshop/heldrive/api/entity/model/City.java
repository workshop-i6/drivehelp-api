package com.workshop.heldrive.api.entity.model;


import com.workshop.heldrive.api.entity.beans.AddressBean;

import javax.persistence.*;

@Entity
@Table(name="City")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name")
    private String nom;
    @Basic
    @Column(name="zipCode",unique = true)
    private String zipcode;

    @ManyToOne
    @JoinColumn(name = "state_id",referencedColumnName = "id")
    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public City() {
    }

    public City(AddressBean addressBean) {
        nom=addressBean.getCity();
        zipcode=addressBean.getZipCode();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
