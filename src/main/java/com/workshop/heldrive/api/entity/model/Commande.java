package com.workshop.heldrive.api.entity.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "Commandes")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;
    @Basic
    @Column(name = "date")
    private LocalDateTime date;
    @Basic
    @Column(name = "total")
    private double total;
    @Basic
    @Column(name = "numero")
    private String numero;
    @Basic
    @Column(name = "status")
    private int status=0; // attente : 0 ,  en cours : 1, terminé : 2

    @ManyToOne
    @JoinColumn(name = "demandeur_id",referencedColumnName = "id")
    private User demandeur;


    @ManyToOne
    @JoinColumn(name = "livreur_id",referencedColumnName = "id")
    private User livreur;

    @OneToMany(mappedBy = "commande",cascade=CascadeType.REMOVE)
    @JsonIgnoreProperties("commande")
    private Collection<LigneCommande> lines;

    public Commande() {
    }



    @Override
    public String toString() {
        return "Commande{" +
                "id=" + id +
                ", date=" + date +
                ", total=" + total +
                ", numero='" + numero + '\'' +
                ", status=" + status +
                ", demandeur=" + demandeur +
                ", livreur=" + livreur +
                ", lines=" + lines +
                '}';
    }

    public User getDemandeur() {
        return demandeur;
    }


    public void setDemandeur(User demandeur) {
        this.demandeur = demandeur;
    }


    public User getLivreur() {
        return livreur;
    }

    public void setLivreur(User livreur) {
        this.livreur = livreur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public Collection<LigneCommande> getLines() {
        return lines;
    }

    public void setLines(Collection<LigneCommande> lines) {
        this.lines = lines;
    }
}
