package com.workshop.heldrive.api.entity;

import com.workshop.heldrive.api.entity.model.Commande;
import com.workshop.heldrive.api.entity.model.User;

import java.util.Collection;

public class DataUser {

    private User user;
    private Collection<Commande> commandesDemandeur;
    private Collection<Commande> commandesLivreur;

    public DataUser() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<Commande> getCommandesDemandeur() {
        return commandesDemandeur;
    }

    public void setCommandesDemandeur(Collection<Commande> commandesDemandeur) {
        this.commandesDemandeur = commandesDemandeur;
    }

    public Collection<Commande> getCommandesLivreur() {
        return commandesLivreur;
    }

    public void setCommandesLivreur(Collection<Commande> commandesLivreur) {
        this.commandesLivreur = commandesLivreur;
    }
}
