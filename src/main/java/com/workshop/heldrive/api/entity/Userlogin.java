package com.workshop.heldrive.api.entity;

import com.workshop.heldrive.api.entity.model.Address;
import com.workshop.heldrive.api.entity.model.User;

import java.io.Serializable;

public class Userlogin implements Serializable {

    private String lastname;
    private String firstname;
    private String mail;
    private String dateOfBirth;
    private Address address;
    private int userId;

    public Userlogin() {

    }

    public Userlogin(User user){
        lastname=user.getLastname();
        firstname=user.getFirstname();
        mail=user.getEmail();
        dateOfBirth=user.getDateOfBirth().toString();
        userId=user.getId();
        address=user.getAddress();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
