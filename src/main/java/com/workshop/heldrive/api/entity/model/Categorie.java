package com.workshop.heldrive.api.entity.model;

import com.workshop.heldrive.api.entity.beans.CategorieBean;

import javax.persistence.*;
import java.util.Collection;

/**
 * Classe produit
 */
@Entity
@Table(name="Categories")
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name",unique = true)
    private String name;
    private String imageName;

    public Categorie() {
    }



    public Categorie(CategorieBean categorieBean) {
        name=categorieBean.getNom();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @OneToMany(mappedBy = "categorie")
    private Collection<Produit> produits;

    public Collection<Produit> getProduits() {
        return produits;
    }

    public void setProduits(Collection<Produit> categories) {
        this.produits = categories;
    }

    @Basic
    @Column(name = "image")
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
