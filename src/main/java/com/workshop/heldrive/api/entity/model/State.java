package com.workshop.heldrive.api.entity.model;


import com.workshop.heldrive.api.entity.beans.AddressBean;

import javax.persistence.*;

@Entity
@Table(name = "State")
public class State {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "name",unique = true)
    private String name;

    public State() {
    }


    public State(AddressBean addressBean) {
        name=addressBean.getCountry();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
