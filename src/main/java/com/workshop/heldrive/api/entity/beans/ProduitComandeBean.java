package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class ProduitComandeBean implements Serializable {

    private int id;
    private String name;
    private double priceMax;
    private int quantity;

    public ProduitComandeBean() {
    }

    public ProduitComandeBean(int id, String name, double priceMax, int quantity) {
        this.id = id;
        this.name = name;
        this.priceMax = priceMax;
        this.quantity = quantity;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(double priceMax) {
        this.priceMax = priceMax;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ProduitComandeBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", priceMax=" + priceMax +
                ", quantity=" + quantity +
                '}';
    }
}
