package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class Login implements Serializable {

    private String mail;
    private String password;

    public Login() {
    }

    public Login(String mail, String password) {
        this.mail = mail;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }



    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
