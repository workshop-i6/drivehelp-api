package com.workshop.heldrive.api.entity.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.workshop.heldrive.api.entity.beans.ProduitComandeBean;

import javax.persistence.*;

@Entity
@Table(name = "ligne_commande")
public class LigneCommande {

    private int id;
    @Basic
    @Column(name = "quantite")
    private int quantite;
    private Commande commande;
    private Produit produit;

    public LigneCommande(){}

    public LigneCommande(ProduitComandeBean line) {
        quantite=line.getQuantity();
    }

    @ManyToOne
    @JoinColumn(name="commande_id",referencedColumnName = "id")
    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @ManyToOne
    @JoinColumn(name = "produit_id",referencedColumnName = "id")
    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
