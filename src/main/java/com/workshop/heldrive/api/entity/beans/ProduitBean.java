package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class ProduitBean implements Serializable {

    private String name;



    private double priceMax;

    public ProduitBean(){}

    public ProduitBean(String nom,double p){this.name =nom;this.priceMax=p;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(double priceMax) {
        this.priceMax = priceMax;
    }


}
