package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class AddressBean implements Serializable {

    private String  numero;
    private String street;
    private String city;
    private String zipCode;
    private String country;
    private String complement;
    private int userId;




    public AddressBean() {}

    public AddressBean(String numero, String street, String city, String zipCode, String country, String complement, int userId) {
        this.numero = numero;
        this.street = street;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
        this.complement = complement;
        this.userId = userId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
