package com.workshop.heldrive.api.entity.beans;

import java.io.Serializable;

public class CategorieBean implements Serializable {

    private String nom;


    public CategorieBean() {
    }

    public CategorieBean(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
