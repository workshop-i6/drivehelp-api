package com.workshop.heldrive.api.entity.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.workshop.heldrive.api.entity.beans.ProduitBean;

import javax.persistence.*;

/**
 * Classe produit
 */
@Entity
@Table(name="Produits")
public class Produit {

    private int id;
    private String nom;
    private double prixMax;
    private Categorie categorie;
    private String imageName;


    public Produit(){}

    public  Produit(ProduitBean prodBean){
        nom=prodBean.getName();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "nom", length = 50, unique = true)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name="prixMax")
    public double getPrixMax() {
        return prixMax;
    }

    public void setPrixMax(double prix) {
        this.prixMax = prix;
    }

    @ManyToOne
    @JoinColumn(name = "categorie_id",referencedColumnName = "id")
    @JsonIgnoreProperties("produits")
    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "image")
    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
