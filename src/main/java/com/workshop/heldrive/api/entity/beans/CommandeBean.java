package com.workshop.heldrive.api.entity.beans;

import com.workshop.heldrive.api.entity.model.LigneCommande;
import com.workshop.heldrive.api.entity.model.User;

import java.io.Serializable;
import java.util.Collection;

public class CommandeBean implements Serializable {

    private Collection<ProduitComandeBean> lines;
    private int demandeur_id;

    public CommandeBean() {
    }


    public Collection<ProduitComandeBean> getLines() {
        return lines;
    }

    public void setLines(Collection<ProduitComandeBean> lines) {
        this.lines = lines;
    }

    public int getDemandeur() {
        return demandeur_id;
    }

    public void setDemandeur(int demandeur) {
        this.demandeur_id = demandeur;
    }

    @Override
    public String toString() {
        return "CommandeBean{" +
                "lines=" + lines +
                ", demandeur_id=" + demandeur_id +
                '}';
    }
}
