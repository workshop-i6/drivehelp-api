package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProduitComandeBeanTest {

    private ProduitComandeBean produitComandeBean;
    private int id=1;
    private String name="lait";
    private int quantite=2;
    private double price=1.2;

    @BeforeEach
    void setUp() {
        produitComandeBean=new ProduitComandeBean(id,name,price,quantite);
    }

    @Test
    void getId() {
        assertEquals(produitComandeBean.getId(),id);
    }

    @Test
    void setId() {
        int id=2;
        produitComandeBean.setId(id);
        assertEquals(produitComandeBean.getId(),id);
    }

    @Test
    void getName() {
        assertEquals(produitComandeBean.getName(),name);
    }

    @Test
    void setName() {
        String n="oeuf";
        produitComandeBean.setName(n);
        assertEquals(produitComandeBean.getName(),n);
    }

    @Test
    void getPriceMax() {
        assertEquals(produitComandeBean.getPriceMax(),price);
    }

    @Test
    void setPriceMax() {
        double price=2.0;
        produitComandeBean.setPriceMax(price);
        assertEquals(produitComandeBean.getPriceMax(),price);
    }


    @Test
    void getQuantity() {
        assertEquals(produitComandeBean.getQuantity(),quantite);
    }

    @Test
    void setQuantity() {
        int q=2;
        produitComandeBean.setQuantity(q);
        assertEquals(produitComandeBean.getQuantity(),q);
    }

}