package com.workshop.heldrive.api.entity.model;

import org.apache.log4j.DailyRollingFileAppender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private User user;
    private int id=1;
    private String username="molinat";
    private String mail="thibautmolina@yahoo.fr";
    private String password="1234";
    private String lastname="molina";
    private String firstname="thibaut";
    private String token="12334";
    private LocalDate dateOfBirth;
    private Date expire;
    private Address address;
    private int idAdresse=1;


    @BeforeEach
    void setUp() {
        user=new User();
        expire=new Date();
        address=new Address();
        address.setId(idAdresse);
        dateOfBirth=LocalDate.now();
        user.setId(id);
        user.setPassword(password);
        user.setExpire(expire);
        user.setAddress(address);
        user.setToken(token);
        user.setFirstname(firstname);
        user.setDateOfBirth(dateOfBirth);
        user.setUsername(username);
        user.setEmail(mail);
        user.setLastname(lastname);
    }

    @Test
    void getLastname() {
        assertEquals(user.getLastname(),lastname);
    }

    @Test
    void getFirstname() {
        assertEquals(user.getFirstname(),firstname);
    }

    @Test
    void getEmail() {
        assertEquals(user.getEmail(),mail);
    }

    @Test
    void getAddress() {
        assertEquals(user.getAddress().getId(),idAdresse);
    }

    @Test
    void getToken() {
        assertEquals(user.getToken(),token);
    }

    @Test
    void getExpire() {
        assertEquals(user.getExpire(),expire);
    }

    @Test
    void getDateOfBirth() {
        assertEquals(user.getDateOfBirth(),dateOfBirth);
    }

    @Test
    void getId() {
        assertEquals(user.getId(),id);
    }
}