package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CityTest {

    private City city;
    private int id=1;
    private String name="Montpellier";
    private String zipCode="34000";
    private State state;
    private int idState=1;

    @BeforeEach
    void setUp() {
        city=new City();
        state=new State();
        city.setId(id);
        state.setId(idState);
        city.setState(state);
        city.setNom(name);
        city.setZipcode(zipCode);
    }

    @Test
    void getState() {
        assertEquals(city.getState().getId(),idState);
    }

    @Test
    void getId() {
        assertEquals(city.getId(),id);
    }

    @Test
    void getNom() {
        assertEquals(city.getNom(),name);
    }

    @Test
    void getZipcode() {
        assertEquals(city.getZipcode(),zipCode);
    }
}