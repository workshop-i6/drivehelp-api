package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserBeanTest {

    private UserBean userBean;
    private String lastname="Thibaut";
    private String firstname="Molina";
    private String mail="thibautmolina@yahoo.fr";
    private String dateOfBirth="2000-01-01";
    private String password="1234";

    @BeforeEach
    void setUp() {
        userBean=new UserBean();
        userBean.setFirstname(firstname);
        userBean.setDateOfBirth(dateOfBirth);
        userBean.setLastname(lastname);
        userBean.setMail(mail);
        userBean.setPassword(password);
    }

    @Test
    void getLastName() {
        assertEquals(userBean.getLastname(),lastname);
    }

    @Test
    void getFirstname() {
        assertEquals(userBean.getFirstname(),firstname);
    }

    @Test
    void getMail() {
        assertEquals(userBean.getMail(),mail);
    }

    @Test
    void getPassword() {
        assertEquals(userBean.getPassword(),password);
    }

    @Test
    void getDateOfBirth() {
        assertEquals(userBean.getDateOfBirth(),dateOfBirth);
    }




}