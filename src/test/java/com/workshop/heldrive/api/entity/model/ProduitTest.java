package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProduitTest {

    private Produit produit;
    private int id=1;
    private String nom="lait";
    private double price=1.2;
    private int idCategorie=2;
    private String image="lait.jpg";

    @BeforeEach
    void setUp() {
        produit=new Produit();
        produit.setId(id);
        Categorie categorie=new Categorie();
        categorie.setId(idCategorie);
        produit.setCategorie(categorie);
        produit.setNom(nom);
        produit.setPrixMax(price);
        produit.setImageName(image);
    }

    @Test
    void getId() {
        assertEquals(produit.getId(),id);
    }

    @Test
    void getNom() {
        assertEquals(produit.getNom(),nom);
    }

    @Test
    void getPrixMax() {
        assertEquals(produit.getPrixMax(),price);
    }

    @Test
    void getCategorie() {
        assertEquals(produit.getCategorie().getId(),idCategorie);
    }

    @Test
    void getImageName() {
        assertEquals(produit.getImageName(),image);
    }
}