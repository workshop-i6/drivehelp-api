package com.workshop.heldrive.api.entity;

import com.workshop.heldrive.api.entity.model.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserloginTest {

    private Userlogin userLogin;

    private String mail="thibautmolina@yahoo.fr";
    private String lastname="molina";
    private String firstname="thibaut";
    private String dateOfbirth="1988-06-17";
    private Address address;
    private int idAdresse=1;
    private int idUser=1;

    @BeforeEach
    void setUp() {
        userLogin=new Userlogin();
        userLogin.setFirstname(firstname);
        userLogin.setDateOfBirth(dateOfbirth);
        address=new Address();
        address.setId(idAdresse);

        userLogin.setAddress(address);
        userLogin.setUserId(idUser);
        userLogin.setMail(mail);
        userLogin.setLastname(lastname);
    }

    @Test
    void getUserId() {
        assertEquals(userLogin.getUserId(),idUser);
    }

    @Test
    void getAddress() {
        assertEquals(userLogin.getAddress().getId(),address.getId());
    }

    @Test
    void getLastname() {
        assertEquals(userLogin.getLastname(),lastname);
    }

    @Test
    void getFirstname() {
        assertEquals(userLogin.getFirstname(),firstname);
    }

    @Test
    void getMail() {
        assertEquals(userLogin.getMail(),mail);
    }

    @Test
    void getDateOfBirth() {
        assertEquals(userLogin.getDateOfBirth(),dateOfbirth);
    }
}