package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageTest {

    private Message message;
    private String str="reussi";

    @BeforeEach
    void setUp() {
        message=new Message();
        message.setMessage(str);
        message=new Message(str);
    }

    @Test
    void getMessage() {
        assertEquals(message.getMessage(),str);
    }
}