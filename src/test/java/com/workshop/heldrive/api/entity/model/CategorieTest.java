package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CategorieTest {

    private Categorie  categorie;
    private String name="food";
    private int id=1;
    private String image="food.jpg";
    private Collection<Produit> produits=new ArrayList<>();
    private Produit produit;
    private int idProduit=1;

    @BeforeEach
    void setUp() {
        produits=new ArrayList<>();
        produit=new Produit();
        produit.setId(idProduit);
        produits.add(produit);
        categorie=new Categorie();
        categorie.setId(id);
        categorie.setImageName(image);
        categorie.setName(name);
        categorie.setProduits(produits);
    }

    @Test
    void getId() {
        assertEquals(categorie.getId(),id);
    }

    @Test
    void getName() {
        assertEquals(categorie.getName(),name);
    }

    @Test
    void getProduits() {
        assertEquals(categorie.getProduits().size(),1);
    }

    @Test
    void getImageName() {
        assertEquals(categorie.getImageName(),image);
    }
}