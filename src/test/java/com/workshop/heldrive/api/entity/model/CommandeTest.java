package com.workshop.heldrive.api.entity.model;

import org.exolab.castor.types.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CommandeTest {

    private Commande commande;
    private int id=1;
    private User demnadeur;
    private User livreur;
    private int idD=1;
    private int idL=2;
    private String numero="1234";
    private LocalDateTime dateTime;
    private Collection<LigneCommande> lines;
    private LigneCommande ligneCommande;
    private int idLine=1;
    private double tot=2.5;



    @BeforeEach
    void setUp() {
        commande = new Commande();
        dateTime = LocalDateTime.now();
        demnadeur=new User();
        demnadeur.setId(idD);
        livreur=new User();
        livreur.setId(idL);
        ligneCommande=new LigneCommande();
        ligneCommande.setId(idLine);
        lines = new ArrayList<>();
        lines.add(ligneCommande);
        commande.setDemandeur(demnadeur);
        commande.setLivreur(livreur);
        commande.setLines(lines);
        commande.setDate(dateTime);
        commande.setId(id);
        commande.setNumero(numero);
        commande.setTotal(tot);
    }

    @Test
    void getDemandeur() {
        assertEquals(commande.getDemandeur().getId(),idD);
    }

    @Test
    void getLivreur() {
        assertEquals(commande.getLivreur().getId(),idL);
    }

    @Test
    void getId() {
        assertEquals(commande.getId(),id);
    }

    @Test
    void getDate() {
        assertEquals(commande.getDate(),dateTime);
    }

    @Test
    void getTotal() {
        assertEquals(commande.getTotal(),tot);
    }

    @Test
    void getNumero() {
        assertEquals(commande.getNumero(),numero);
    }

    @Test
    void getStatus() {
        assertEquals(commande.getStatus(),0);
    }

    @Test
    void getLines() {
        assertEquals(commande.getLines().size(),1);
    }
}