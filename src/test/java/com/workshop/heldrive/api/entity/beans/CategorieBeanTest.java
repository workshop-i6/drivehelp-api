package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CategorieBeanTest {

    private CategorieBean categorieBean;
    private String name="Food";

    @BeforeEach
    void setUp() {
        categorieBean=new CategorieBean();
        categorieBean=new CategorieBean(name);
    }

    @Test
    void getNom() {
        assertEquals(categorieBean.getNom(),name);
    }

    @Test
    void setNom() {
        String str="hygiene";
        categorieBean.setNom(str);
        assertEquals(categorieBean.getNom(),str);
    }
}