package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoginTest {

    private Login login;
    private String mail="thibautmolina@yahoo.fr";
    private String password="1234";

    @BeforeEach
    void setUp() {
        login=new Login();
        login=new Login(mail,password);
    }

    @Test
    void getPassword() {
        assertEquals(login.getPassword(),password);
    }

    @Test
    void setPassword() {
        String pn="4567";
        login.setPassword(pn);
        assertEquals(login.getPassword(),pn);
    }

    @Test
    void getMail() {
        assertEquals(login.getMail(),mail);
    }

    @Test
    void setMail() {
        String pn="thibautmolina";
        login.setMail(pn);
        assertEquals(login.getMail(),pn);
    }
}