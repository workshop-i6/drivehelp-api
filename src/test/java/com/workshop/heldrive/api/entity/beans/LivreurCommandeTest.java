package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LivreurCommandeTest {

    private LivreurCommande livreurCommande;
    private int idLivreur=1;

    private int idCommande=1;

    @BeforeEach
    void setUp() {
        livreurCommande=new LivreurCommande();
        livreurCommande=new LivreurCommande(idLivreur,idCommande);
    }

    @Test
    void getIdLivreur() {
        assertEquals(livreurCommande.getIdLivreur(),idLivreur);
    }

    @Test
    void setIdLivreur() {
        int nid=3;
        livreurCommande.setIdLivreur(nid);
        assertEquals(livreurCommande.getIdLivreur(),nid);
    }

    @Test
    void getIdCommande() {
        assertEquals(livreurCommande.getIdCommande(),idCommande);
    }

    @Test
    void setIdCommande() {
        int nid=3;
        livreurCommande.setIdCommande(nid);
        assertEquals(livreurCommande.getIdCommande(),nid);
    }
}