package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StateTest {

    private State state;
    private int id=1;
    private String country="France";

    @BeforeEach
    void setUp() {
        state=new State();
        state.setId(id);
        state.setName(country);
    }

    @Test
    void getId() {
        assertEquals(state.getId(),id);
    }

    @Test
    void getName() {
        assertEquals(state.getName(),country);
    }
}