package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    private  Address address;
    private int id=1;
    private String street="rue du petit houx";
    private String complement="apt 46";
    private User user;
    private int idUser=1;
    private String numero="11";
    private City city;
    private int idCity=1;

    @BeforeEach
    void setUp() {
        user=new User();
        user.setId(idUser);
        city=new City();
        city.setId(idCity);
        address=new Address();
        address.setId(id);
        address.setUser(user);
        address.setStreet(street);
        address.setComplement(complement);
        address.setCity(city);
        address.setNumero(numero);
    }

    @Test
    void getId() {
        assertEquals(address.getId(),id);
    }

    @Test
    void getNumero() {
        assertEquals(address.getNumero(),numero);
    }

    @Test
    void getStreet() {
        assertEquals(address.getStreet(),street);
    }

    @Test
    void getComplement() {
        assertEquals(address.getComplement(),complement);
    }

    @Test
    void getUser() {
        assertEquals(address.getUser().getId(),idUser);
    }

    @Test
    void getCity() {
        assertEquals(address.getCity().getId(),idCity);
    }




}