package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProduitBeanTest {

    private ProduitBean produitBean;
    private String name="lait";
    private double price=1.2;

    @BeforeEach
    void setUp() {
        produitBean=new ProduitBean();
        produitBean=new ProduitBean(name,price);
    }



    @Test
    void getName() {
        assertEquals(produitBean.getName(),name);
    }

    @Test
    void setName() {
        String n="oeuf";
        produitBean.setName(n);
        assertEquals(produitBean.getName(),n);
    }

    @Test
    void getPriceMax() {
        assertEquals(produitBean.getPriceMax(),price);
    }

    @Test
    void setPriceMax() {
        double p=1.0;
        produitBean.setPriceMax(p);
        assertEquals(produitBean.getPriceMax(),p);
    }


}