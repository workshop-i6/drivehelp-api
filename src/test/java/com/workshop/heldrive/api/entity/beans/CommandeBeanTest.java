package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CommandeBeanTest {

    private CommandeBean commandeBean;
    private ProduitComandeBean produitComandeBean;
    private int idDemandeur;
    private Collection<ProduitComandeBean> produitComandeBeans=new ArrayList<>();
    private int idProduit;

    @BeforeEach
    void setUp() {
        produitComandeBean=new ProduitComandeBean();
        produitComandeBean.setId(idProduit);
        produitComandeBeans.add(produitComandeBean);
        commandeBean=new CommandeBean();
        commandeBean.setDemandeur(idDemandeur);
        commandeBean.setLines(produitComandeBeans);
    }

    @Test
    void getLines() {
        assertEquals(commandeBean.getLines().size(),1);
    }



    @Test
    void getDemandeur() {
        assertEquals(commandeBean.getDemandeur(),idDemandeur);
    }


}