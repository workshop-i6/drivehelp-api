package com.workshop.heldrive.api.entity.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressBeanTest {

    private AddressBean addressBean;
    private String numero="123456";
    private String street="rue du petit houx";
    private String city="Montpellier";
    private String zipcode="34070";
    private String country="France";
    private String complement="apt 46";
    private int userId=1;


    @BeforeEach
    void setUp() {
        addressBean=new AddressBean();
        addressBean=new AddressBean(numero,street,city,zipcode,country,complement,userId);
    }

    @Test
    void getNumero() {
        assertEquals(addressBean.getNumero(),numero);
    }

    @Test
    void setNumero() {
        String s="123";
        addressBean.setNumero(s);
        assertEquals(addressBean.getNumero(),s);
    }

    @Test
    void getStreet() {
        assertEquals(addressBean.getStreet(),street);
    }

    @Test
    void setStreet() {
        String str="rue de la paix";
        addressBean.setStreet(str);
        assertEquals(addressBean.getStreet(),str);
    }

    @Test
    void getCity() {
        assertEquals(addressBean.getCity(),city);
    }

    @Test
    void setCity() {
        String ci="Grabels";
        addressBean.setCity(ci);
        assertEquals(addressBean.getCity(),ci);
    }

    @Test
    void getZipCode() {
        assertEquals(addressBean.getZipCode(),zipcode);
    }

    @Test
    void setZipCode() {
        String ci="34790";
        addressBean.setZipCode(ci);
        assertEquals(addressBean.getZipCode(),ci);
    }

    @Test
    void getCountry() {
        assertEquals(addressBean.getCountry(),country);
    }

    @Test
    void setCountry() {
        String ci="Cote d'ivoire";
        addressBean.setCountry(ci);
        assertEquals(addressBean.getCountry(),ci);
    }

    @Test
    void getComplement() {
        assertEquals(addressBean.getComplement(),complement);
    }

    @Test
    void setComplement() {
        String ci="apt 32";
        addressBean.setComplement(ci);
        assertEquals(addressBean.getComplement(),ci);
    }

    @Test
    void getUserId() {
        assertEquals(addressBean.getUserId(),userId);

    }

    @Test
    void setUserId() {
       int id=2;
        addressBean.setUserId(id);
        assertEquals(addressBean.getUserId(),id);
    }
}