package com.workshop.heldrive.api.entity.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LigneCommandeTest {

    private LigneCommande ligneCommande;
    private int id=1;
    private int quantite=2;
    private int idCommande=1;
    private int idProduit;

    @BeforeEach
    void setUp() {
        ligneCommande=new LigneCommande();
        ligneCommande.setId(id);
        Produit prod=new Produit();
        prod.setId(idProduit);
        ligneCommande.setProduit(prod);
        Commande commande=new Commande();
        commande.setId(idCommande);
        ligneCommande.setCommande(commande);
        ligneCommande.setQuantite(quantite);
    }

    @Test
    void getCommande() {
        assertEquals(ligneCommande.getCommande().getId(),idCommande);
    }

    @Test
    void getProduit() {
        assertEquals(ligneCommande.getProduit().getId(),idProduit);
    }

    @Test
    void getId() {
        assertEquals(ligneCommande.getId(),id);
    }

    @Test
    void getQuantite() {
        assertEquals(ligneCommande.getQuantite(),quantite);
    }
}