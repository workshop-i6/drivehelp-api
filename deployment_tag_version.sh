TAG_VERSION="$1"
DEPLOYMENT_FILE_PATH="$2"


if [ "$2" == "" ]; then
    echo "Missing parameter! Parameters are TAG_VERSION and DEPLOYMENT_FILE_PATH.";
    exit 1;
fi

cat > ${DEPLOYMENT_FILE_PATH} <<EOL
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mspr
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: msprintegration
    spec:
      containers:
        - name: msprintegration
          image: registry.gitlab.com/mspr-mise-en-oeuvre-d-une-integration-continue/server-ci:$TAG_VERSION
          imagePullPolicy: Always
          ports:
            - containerPort: 8080
      imagePullSecrets:
        - name: registry.gitlab.com
EOL
