FROM java:8

WORKDIR /

ADD target/integrationContinue-0.0.1-SNAPSHOT.jar integrationContinue.jar

EXPOSE 8080

CMD java -jar integrationContinue.jar


